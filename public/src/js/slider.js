$(document).ready(function(){
	var winH = $(window).height();
	$('.main-visual, .about-1 .visual__wrap').height(winH);

	$(window).load(function(){

		var visualNum = $('.visual__item').length
		if(visualNum == 1){$('.visual__btn').hide();}
		youtube_play_api();
		function youtube_play_api(){
		    "use strict";
			var videoId = ['tDaUDStUERk', 'TVUqLBRQom8', 'neFnl-MqfxA'];

			$('.js-play').on('click', function(e){
			  var index = $(this).parents('.visual__item').index()-2;
			  createVideo(videoId[index]);
			});
		    var tag = document.createElement('script');

		    tag.src = "https://www.youtube.com/iframe_api";
		    var firstScriptTag = document.getElementsByTagName('script')[0];
		    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

			var player;
			function createVideo(id) {
				if(player){
				player.loadVideoById(id);
					return;
				}
				player = new YT.Player('youtube_video', {
					videoId: id,
					playerVars: { 'autoplay': 1, 'controls': 0 },
					events: {
						'onReady': onPlayerReady
					}
				});
			}

			function onPlayerReady(event) {
			  event.target.playVideo();
			}

		    $(".youtube__bg, .js-popup-close").click(function(){
		         player.pauseVideo();
		    });


			$(window).on("load resize", function () {
				if ($('body').attr('data-mobile') == 'false'){

				} else {
					// player.pauseVideo();
					$('.youtube').removeClass('is-active');
				}
			});

		}
		$('.js-main').cycle({
			fx:'scrollHorz',
			swipe:true,
		    slides:'.visual__item',
			prev:'.js-prev',
			next:'.js-next',
			pager:'.visual__page'
		});
		$('.js-festival').cycle({
			fx:'scrollHorz',
			swipe:true,
		    slides:'.visual__item',
			prev:'.js-prev',
			next:'.js-next',
			continueAuto : false,
			pager:'.visual__page'
		});

	});

	var i = 0;
	$('.visual__play').click(function(e) {
		e.preventDefault();
	    if (i == 0){
	        $('.js-main').cycle('pause');
	        $(this).attr('class','visual__play is-pause');
	        i++;
	    }
	    else{
	        $('.js-main').cycle('resume');
	        $(this).attr('class','visual__play is-play');
	        i--;
	    }
	});



	/*슬라이드*/
	$('.js-play').on('click',function(e){
		e.preventDefault();
		$('.pop-youtube').addClass('is-active');
		$('.js-main').cycle('pause');

	});
	$('.youtube__bg').on('click',function(e){
		e.preventDefault();
		$('.pop-youtube').removeClass('is-active');
		$('.js-main').cycle('resume');
	});
	$('.js-play-m').on('click',function(e){
		e.preventDefault();
		$('.pop-youtube').addClass('is-active');
		$('.js-main').cycle('pause');
	});
	$('.js-popup-close').on('click',function(e){
		e.preventDefault();
		$('.pop-youtube').removeClass('is-active');
		$('.js-main').cycle('resume');

			// var video = $(this).parents('.pop-youtube__box').find('.pop-youtube__video').attr("src");
			// $(this).parents('.pop-youtube__box').find('.pop-youtube__video').attr("src","");
			// $(this).parents('.pop-youtube__box').find('.pop-youtube__video').attr("src",video);


	});
});
$(window).on("load resize", function () {
	$('body').attr('data-mobile',
		(function(){
			var r = ($(window).width() <= 1025) ? true : false;
			return r;
		})
	);
	if ($('body').attr('data-mobile') == 'false'){
		$('.video1').css('left',-reyouW);
		// $('.js-slide3').slick('unslick');
	} else {
	}
	var winH = $(window).height();
	var winW = $(window).width();
	var youtubeW = $('.youtube iframe').width();
	var reyouW = (1900 - winW)/2

	$('.basic__box').each(function(index){
		$(this).attr("data-index",winH * index);
	});






	$('.main-visual, .about-1 .visual__wrap').height(winH);
	$('.visual').on("mousewheel DOMMouseScroll touchmove",function(e){
		/*파폭 동일하게 시켜주기*/
		var E = e.originalEvent;

		delta = 0;
		if (E.detail) {
			delta = E.detail * -40;
		}else{
			delta = E.wheelDelta;
		};
		delta = E.wheelDelta;
		/* //파폭 동일하게 시켜주기 */
		var sectionPos = parseInt($(this).attr("data-index"));

		if(delta >= 0) {

			$("html,body").stop().animate({scrollTop:sectionPos - winH});

		return false;

		} else if (delta < 0) {

			$("html,body").stop().animate({scrollTop:sectionPos + winH});

		return false;

		}

	});

	// $(function(){
	//         $('.visual').swipe({
	//
	//             swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
	//                 if( direction == "up" ){
	//                     $("html,body").stop().animate({scrollTop:winH});
	// 					threshold:0
	//                 }else if( direction == "down" ){
	// 					$("html,body").stop().animate({scrollTop:0});
	// 					threshold:0
	//             }
	//         },
	//     });
	// });



}).resize();
