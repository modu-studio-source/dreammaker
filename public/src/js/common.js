



/*gnb*/
$(function() {
	var gnb = $('#gnb');
	var gnbState = 0;
	$(window).on("load resize", function () {
		/*레이아웃잡기*/
		var footerH = $('.footer').outerHeight();
		$('.main, .content').css('padding-bottom',footerH);
		$('.footer').css('margin-top',-footerH);
		if ($('body').attr('data-mobile') == 'false'){
			$(gnb).removeClass('js-open-m').find("li").removeClass('js-open-m');
			$('.js-dim').removeClass('is-active');
			$('.tab-v1__list').removeClass('js-open-m');
		} else {
			$(gnb).removeClass('js-open-d').find("li").removeClass('js-open-d');
			$('.gnb__depth2-item').removeClass('is-active');
		}
	});

	$(gnb.selector+">ul>li>a").on("click mouseenter focus touchstart",function(e) {
		if ($('body').attr('data-mobile') == 'false') {
			$('.gnb__depth1-item').addClass('js-open-d');
			$(gnb).addClass('js-open-d');
			$('.main-search').removeClass('is-active');
		} else {
			if(e.type == "click"){
				e.preventDefault();
				if ($(this).parents("li").hasClass("js-open-m")) {
					$(this).parents("li").removeClass("js-open-m");
				} else {
					$(this).parents("li").siblings().removeClass("js-open-m");
					$(this).parents("li").addClass("js-open-m");
				}
			}
		}
	});
	$(gnb).on("mouseleave",function() {
		$(this).removeClass('js-open-d');
		//$(gnb).find("[class$=tit]").stop().animate({height:'0'},300);
		$(this).find('li').removeClass('js-open-d js-first');
		gnbState = 0;
	});
	$(gnb).find("a").last().on("blur",function() {
		$(gnb).trigger("mouseleave");
	});
	$(gnb).find("[class$=tit]").on("click" ,function(){
		if ($('body').attr('data-mobile') == "false") return false;
		if (!$(gnb).hasClass("js-open-m")) {
			$(gnb).addClass('js-open-m');
		} else {
			$(gnb).removeClass('js-open-m');
		}
		$('.logo, .js-quick').addClass('is-hidden');
	});
	$(gnb).find('.gnb__depth2-link').on("click" ,function(e){
		var gnbData = $(this).attr('data-depth');
		var gnbHeight = $(this).attr('data-height');
		if (gnbData == 'true'){
			e.preventDefault();
			if ($(this).parents(".gnb__depth2-item").hasClass("is-active")) {
				$(this).parents(".gnb__depth2-item").removeClass("is-active");
				$('.gnb__tit').removeClass('is-active');
			} else {
				$(this).parents(".gnb__depth2-item").siblings().removeClass("is-active");
				$(this).parents(".gnb__depth2-item").addClass("is-active");
				if(gnbHeight == 'true'){$('.gnb__tit').addClass('is-active');}
			}
		}
		var gnbClass = $(this).attr('class');
		var gnbClass2 = gnbClass.split(' ');
		if ($('body').attr('data-mobile') == "false") return true;
		if (gnbClass2[1] == "disabled") return true;
		if(e.type == "click"){
			e.preventDefault();
			if ($(this).parents(".gnb__depth2-item").hasClass("js-open-m")) {
				$(this).parents(".gnb__depth2-item").removeClass("js-open-m");
			} else {
				$(this).parents(".gnb__depth2-item").siblings().removeClass("js-open-m");
				$(this).parents(".gnb__depth2-item").addClass("js-open-m");
			}
		}
	});
	$('.gnb__menu-close').on('click',function(e){
		e.preventDefault();
		$(gnb).removeClass('js-open-m');
		$('.logo, .js-quick').removeClass('is-hidden');
	});
	$(window).on("scroll", function(e) {
		if ($('body').attr('data-mobile') == "false") return false;
		var wrap = $(gnb).parent();
		if ($(this).scrollTop() > $(wrap).height()) {
			$(wrap).addClass("js-fixed");
			$("#familySite").removeClass("js-open-m");
		} else {
			$(wrap).removeClass("js-fixed");
		}
	});
});

$(document).ready(function(){

	$(".select__box a").click(function(e){
		e.preventDefault();
		var select1 = $(this).parents('.select__wrap').find('.select__box');
		var select2 = $(this).parents('.select__wrap').find('.select__menu');

		select1.toggleClass("is-active");
		select2.toggleClass("is-active");
		$(".select__btn").click(function(e){
			e.preventDefault();
			var select1 = $(this).parents('.select__wrap').find('.select__box');
			var select2 = $(this).parents('.select__wrap').find('.select__menu');
			var select3 = $(this).parents('.select__wrap').find('.select__box a');
			select3.text($(this).text());
			select1.removeClass("is-active");
			select2.removeClass("is-active");
		});
	});
	//달력
	$('.js-calender').on('click',function(){
		$(this).toggleClass('is-active');
		$('.calendar__select').toggleClass('is-active');
	});
	//달력 좀더 연구
	var posY;
	var num = 1;
	$('.js-calender').on('click',function(e){
		e.preventDefault();
		if(num == 1){
			posY = $(window).scrollTop();
			$('html, body').addClass("is-active");
			$('.body__wrap').css({"position":"relative","top":-posY});
			return num = 0;
		}else{
			$('html, body').removeClass("is-active");
			$('.body__wrap').css("position","static")
			posY = $(window).scrollTop(posY);
			return num = 1;
		}
	});
	var year = 2017;
	$('.js-year-select').find('.select__btn').on('click',function(e){
		e.preventDefault();
		$('.js-year-select').find('.select__btn').removeClass('is-active');
		$(this).toggleClass('is-active');
		return year = $(this).text();
	});
	$('.js-month-select').find('.select__btn').on('click',function(e){
		e.preventDefault();
		var month = $(this).text();
		$('.js-month-select').find('.select__btn').removeClass('is-active');
		$(this).toggleClass('is-active')
		$('.calendar__select, .js-calender').removeClass('is-active');
		$('.js-month').text(month);
		$('.js-year').text(year);
		$('html, body').removeClass("is-active");
		$('.body__wrap').css("position","static")
		posY = $(window).scrollTop(posY);
		return num = 1;
	});
	//scroll
	$(".js-scroll").mCustomScrollbar({
		theme:'dark-2'
	});
	//gnb
	$('.gnb__depth1-link').on('mouseenter',function(){
		$(this).addClass('is-active');
	}).on('mouseleave',function(){
		$(this).removeClass('is-active');
	});
	//global
	$('.js-global').on('click',function(){
		$(this).parents('.global__wrap').toggleClass('is-active');
	});
	//news
	$('.list-v1__link').on('mouseenter',function(){
		$(this).siblings('.list-v1__hover').addClass('is-active');
	}).on('mouseleave',function(){
		$(this).siblings('.list-v1__hover').removeClass('is-active');
	});

	$('.list-v4__link').on('mouseenter',function(){
		$(this).siblings('.list-v4__hover').addClass('is-active');
	}).on('mouseleave',function(){
		$(this).siblings('.list-v4__hover').removeClass('is-active');
	});
	/*tab*/
	$(window).scroll(function(){
		if($(this).scrollTop()>0){
			$('.tab-v1__list').addClass('is-active');
			$('.js-tab').removeClass('type-white');
		}else{
			$('.tab-v1__list').removeClass('is-active');
			$('.js-tab').addClass('type-white');
		}
	});

	$('.js-slide1').slick({
	  dots: false,
	  infinite: true,
	  speed: 500,
	  slidesToShow: 3,
	  slidesToScroll: 1,
	  prevArrow:'.js-schedule-prev',
	  nextArrow:'.js-schedule-next',
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
			centerMode:true,
			centerPadding:'0px 105px 0px 0px',
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	  ]
	});
	/*콘서트*/
	$('.js-slide2').slick({
	  dots: false,
	  infinite: true,
	  speed: 500,
	  slidesToShow: 4,
	  slidesToScroll: 1,
	  prevArrow:'.js-schedule-prev',
	  nextArrow:'.js-schedule-next',
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
			centerMode:true,
			centerPadding:'0px 105px 0px 0px',
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	  ]
	});
	// $('.js-slide3').slick({
	// 	arrows:false,
	// 		slidesToShow: 3,
	//   responsive: [
	//
	// 	{
	// 	  breakpoint: 1024,
	// 	  settings: {
	// 		arrows: false,
	// 		slidesToShow: 2,
	// 		slidesToScroll: 2
	// 	  }
	// 	},
	// 	{
	// 	  breakpoint: 480,
	// 	  settings: {
	// 		  infinite: false,
	// 		arrows: false,
	// 		centerMode:true,
	// 		centerPadding:'0px 105px 0px 0px',
	// 		slidesToShow: 1,
	// 		slidesToScroll: 1
	// 	  }
	// 	}
	//   ]
	// });
});

//tab

$(function() {
	$('.tab-v1__link').on('click',function(e){
		e.preventDefault();
		var $tab1 = $(this).parents('.tab-v1__item');
		var num = $tab1.index();
		$(this).parents('.tab-v1__item').addClass('is-active').siblings().removeClass('is-active');
		$('.tab-v1__content').eq(num).addClass('is-active').siblings().removeClass('is-active');
	});
});


//faq모션
// $(function(){
// 	$('.list-v2__item:first-child').addClass('is-active').find('.list-v2__content').css('display','block');
// 	$(".list-v2__header").on("click keypress",function(e){
// 		var $target = $(this).parent("li");
// 		if ((e.keyCode == 13)||(e.type == 'click'))$(this).parent("li").toggleClass('is-active').siblings().removeClass('is-active');
// 		if ( $target.is('.is-active')){
// 			$(this).next().slideDown('500');
// 			$target.siblings().find('.list-v2__content').slideUp('500');
// 		} else {
// 			$(this).next().slideUp('500');
// 		}
// 	});
// });

//faq모션 X
$(function(){
	$('.list-v2__item:first-child').addClass('is-active').find('.list-v2__content').css('display','block');
	$(".list-v2__header").on("click keypress",function(e){
		var $target = $(this).parent("li");
		if ((e.keyCode == 13)||(e.type == 'click'))$(this).parent("li").toggleClass('is-active').siblings().removeClass('is-active');
		if ( $target.is('.is-active')){
			$(this).next().show();
			$target.siblings().find('.list-v2__content').hide();
		} else {
			$(this).next().hide();
		}
	});
});

//콘서트 말줄임
$(window).on('load',function(){
	$(function() {
		$('.list-v5__wrap').dotdotdot({
			height:90
		});
	});
});
